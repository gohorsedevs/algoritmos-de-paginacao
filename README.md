# Leia-me #

### Contribuidores ###

* Paulo de Tarso Salgado Junior
* Vitor Giavarina Delgallo

### Sobre o trabalho ###

* Trabalho de algoritmos de paginação em JS e JQuery
* Para que o sistema funcione adequadamente em localhost 
deve-se criar um arquivo "constantsPath.php" na pasta 
"/application/config/" e definir uma constante no PHP 
para a pasta de referência do seu projeto no htdocs,
exemplo, se a sua pasta do projeto chama-se 
"algoritmos_paginacao", então deve-se adicionar o seguinte
comando: defined('PATH') OR define('PATH', '/algoritmos_paginacao');
* Se tiver definido uma DNS para uso da aplicação, defina
a constante vazia: defined('PATH') OR define('PATH', '');